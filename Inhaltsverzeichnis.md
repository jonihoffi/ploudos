# PloudOS Support standart answers

## Templates

1. Vorlage Deutsch
	- [[Ticket Answer Template#Deutsch | Deutsches Ticket Template]]
	- [[Template normal answer (NO TICKET)#Deutsch | Deutsche Antwort Template (No Ticket)]]

2. Template English
	- [[Ticket Answer Template#English | English answer Ticket]]
	- [[Template normal answer (NO TICKET) | English answer Template (No Ticket)]]

## Deutsch

1. Spielern in Minecraft permissions für ein plugin geben
	- [[Permissions in Minecraft#Deutsch | Deutsche antwort]]
		- Antwort mit empfehlung für Luckperms
		- Antwort mit warnung vor OP Rechten
2. User permssions um den PloudOS Server zum verwalten geben
	- [[Permissions PloudOS#Deutsch | Deutsche antwort]]
		- Schritt erklärung
		- Warnung vor zu vielen Rechten
3. PloudOS Server status Error
	- [[Server Error#Deutsch | Deutsche antwort]]
		- 2 möglichkeiten das problem zu beheben
		- Erklärung wie man den Support code kriegt
4. PloudOS Server wurde gelöscht
	- [[My Server got Deleted can i get it back#Deutsch | Deutsche antwort]]
		- Erklärung das der Server gelöscht wurde und nicht wieder hergestellt werden kann
		- 2 möglichkeiten um das löschen vorzubeugen
5. PloudOS mehr RAM
	- [[How to get more RAM#Deutsch | Deutsche antwort]]
		- Alle 3 möglichkeiten um mehr ram zu kriegen


## English

1. Give a player a specific permission to use a plugin
	- [[Permissions in Minecraft#Englisch | English answer]]
		- Answer with recommendation
		- Answer with warning against OP rights
2. Give a user permissions to manage the PloudOS Server
	- [[Permissions PloudOS#Englisch | English answer]]
		- Step explanation
		- Warning against too many rights
3. PloudOS Server error state
	- [[Server Error#Englisch | English answer]]
		- 2 possibilities to fix the problem
		- Explanation of how to get the support code
4. PloudOS Server got deleted
	- [[My Server got Deleted can i get it back#Englisch | English answer]]
		- Explanation that the server has been deleted and cannot be restored
		- 2 ways to prevent deletion
5. PloudOS get more RAM
	- [[How to get more RAM#Englisch | English answer]]
		- All 3 ways to get more ram