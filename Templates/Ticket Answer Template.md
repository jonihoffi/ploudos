# Ticker answer template

## Deutsch

```js
Hey erstmal danke das du dich an den support gewendet hast, jetzt aber zu deinem problem. <Falls du willst kurze einleitung zum problem>

<Beschreibung/Text>

Wenn du keine fragen mehr haben solltest schließe das Ticket mit "!close", wenn noch fragen offen sein sollten helfe ich dir gerne weiter.
```

## English

```js
Hey first of all thanks for contacting support, but now to your problem.<If you want short introduction to the problem> 

<Beschreibung/Text>

If you don't have any more questions close the ticket with "!close", if there are still questions I'll be happy to help you.
```