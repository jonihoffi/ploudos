## Permissions PloudOS

# Deutsch

```js
Hey erstmal danke das du dich an den support gewendet hast, jetzt aber zu deinem problem. Wenn du jemanden rechte zum managen deines Servers auf PloudOS geben möchtest folgen den unten gelisteten Schritten.

1. Gehe auf PloudOS.com und navigiere zu dem Server wo du deinem Freund rechte geben möchtest

2. Wechsel zu dem Tab Server teilen.

3. Nun gebe den PloudOS nutzernamen von deinem Freund an und wähle alle rechte die dein freund haben soll. Aber sei vorsichtig mit den falschen rechten kann dein freund deinem server schaden.

4. Als letztes klicke auf Teilen und dein Freund sollte nun deinen Server starten stoppen und managen können.

Wenn du keine fragen mehr haben solltest schließe das Ticket mit "!close", wenn noch fragen offen sein sollten helfe ich dir gerne weiter.
```

# English

```js
Hey first of all thanks for contacting support, but now to your problem. If you want to give someone permissions to manage your server on PloudOS follow the steps listed below.

1. go to PloudOS.com and navigate to the server where you want to give your friend permissions

2. go to the Share Server tab.

3. now enter the PloudOS username of your friend and select all the permissions you want your friend to have. But be careful with the wrong rights, your friend can damage your server.

4. finally click on share and your friend should now be able to start, stop and manage your server.

If you don't have any more questions close the ticket with "!close", if there are still questions I'll be happy to help you.
```