# Server deletet

## Deutsch

```js
Hey erstmal danke das du dich an den support gewendet hast, jetzt aber zu deinem problem. Wenn du deinen Server gelöscht hast ist dieser auch wirklich gelöscht. Das heißt das dein Server leider verloren ist. Aber wie kannst du diesem problem vorbeugen?

1. Erstelle Backups und lade sie auf deinen PC um bei einer Server löschung deine Daten trotzdem gesichert zu haben.

2. Sobald du länger als 60 Tage nicht mehr aktiv warst schicken wir dir eine Email mit dem Hinweis das dein Server in weiteren 30 Tagen gelöscht wird wenn du dich nicht einloggst. Habe also dein Email Postfach im Blick um des automatische löschen der Server von unserem System vorzubeugen.

Wenn du keine fragen mehr haben solltest schließe das Ticket mit "!close", wenn noch fragen offen sein sollten helfe ich dir gerne weiter.
```

## English

```js
Hey first of all thanks for contacting support, but now to your problem. If you have deleted your server, it is really deleted. This means that your server is unfortunately lost. But how can you prevent this problem?

1. make backups and download them to your PC to have your data saved in case of server deletion.

2. as soon as you have not been active for more than 60 days we will send you an email with the notice that your server will be deleted in another 30 days if you do not log in. So keep an eye on your email inbox to prevent the automatic deletion of the server from our system.

If you don't have any more questions close the ticket with "!close", if there are still questions I'll be happy to help you.
```