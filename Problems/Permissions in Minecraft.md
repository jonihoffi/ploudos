# Permissions

## Deutsch

```js
Hey erstmal danke das du dich an den support gewendet hast, jetzt aber zu deinem problem. Wenn du jemanden rechte auf deinem Minecraft Server geben willst gibt es 2 möglichkeiten.

1. Nutze ein Permissions plugins wie zum beispiel "Luckperms" und gebe personen gewisse permissions 

2. Gebe dem Spieler OP beachte aber das er dadurch alle rechte hat und vllt auch deinem Server damit schaden kann.

Wenn du keine fragen mehr haben solltest schließe das Ticket mit "!close", wenn noch fragen offen sein sollten helfe ich dir gerne weiter.
```

## English

```js
Hey, first of all, thanks for contacting support, but now to your problem. If you want to give someone rights on your Minecraft server, there are 2 possibilities.

1. use a permissions plugin like "Luckperms" and give certain permissions to people. 

2. give the player OP but note that he has all the rights and may also harm your server.

If you don't have any more questions close the ticket with "!close", if there are still questions I'll be happy to help you.
```