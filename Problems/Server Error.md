# Server ERROR

## Deutsch
```js
Hey erstmal danke das du dich an den support gewendet hast, jetzt aber zu deinem problem. Das dein Server status error anzeigt kann ab und zu passieren aber keine sorge alle deine Welten und Spielerdaten bleiben unversehrt. Aber wie fixt du das problem?

1. Du wartest 15 minuten danach sollte sich der error von selbst behoben haben oder
2. Du antwortest auf diese Nachricht mit deinem suport code (Diesen kriegst du wenn du `alt + h` auf der PloudOS webseite drückst) und ein Supporter behebt das problem für dich

Wenn du keine fragen mehr haben solltest schließe das Ticket mit "!close", wenn noch fragen offen sein sollten helfe ich dir gerne weiter.
```

## English
```js
Hey, first of all, thanks for contacting support, but now to your problem. 
The server status error can happen from time to time, but don't worry, all your worlds and player data will remain intact. But how do you fix the problem?

1. just wait 15 minutes after that the error should have fixed itself or
2. reply to this message with your suport code (you can get it by pressing `alt + h` on the PloudOS website) and a supporter will fix the problem for you.

If you don't have any more questions close the ticket with "!close", if there are still questions I'll be happy to help you.
```