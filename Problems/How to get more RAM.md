# More RAM

## Deutsch

```js
Hey erstmal danke das du dich an den support gewendet hast, jetzt aber zu deinem problem. Es gibt mehrere möglichkeiten mehr RAM für deinen Server zu kriegen.

1. Verbinde einfach dein PloudOS-Konto mit deinem Discord-Konto. (Gehen dazu auf den PloudOS Discord und gebe `!connect` in #general ein und schaue danach in deinen dms nach)

2. Deaktiviere deinen Adblocker auf der PloudOS webseite oder,

3. Booste unseren Discord Server um 1GB mehr RAM zu erhalten.

Wenn du keine fragen mehr haben solltest schließe das Ticket mit "!close", wenn noch fragen offen sein sollten helfe ich dir gerne weiter.
```

## English

```js
Hey first of all thanks for contacting support, but now to your problem. There are several ways to get more RAM for your server.

1. just connect your PloudOS account with your Discord account. (Go to the PloudOS Discord and type `!connect` in #general and then look in your dms).

2. disable your adblocker on the PloudOS website or,

3. boost our Discord server to get 1GB more RAM.

If you don't have any more questions close the ticket with "!close", if there are still questions I'll be happy to help you.
```